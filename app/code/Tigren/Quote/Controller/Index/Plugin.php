<?php
namespace Tigren\Quote\Controller\Index;

use Magento\Framework\App\Action\Action;

class Plugin extends Action{

    protected $title;

    public function execute()
    {
        echo $this->setTitle('Welcome');
        echo '<pre>';
        echo $this->getTitle();
    }
    public function setTitle($title)
    {
        return $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

}