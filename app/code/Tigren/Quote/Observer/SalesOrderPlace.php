<?php

namespace Tigren\Quote\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Customer;


/**
 * Class TigrenOderSave
 * @package Tigren\Quote\Observer
 */
class SalesOrderPlace implements ObserverInterface
{
    /**
     * @var Cart
     */
    protected $_cart;

    /**
     * @var Customer
     */
    protected $_customer;

    /**
     * TigrenOderSave constructor.
     * @param Customer $customer
     * @param Cart $cart
     */
    public function __construct(
        Customer $customer,
        Cart $cart
    )
    {
        $this->_customer = $customer;
        $this->_cart = $cart;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $sales = $observer->getData('order');
        $idCustomer = $sales->getData('customer_id');
        $vipQuote = $this->_cart->getQuote()->getData('is_vip');
        $sales->setData('is_vip', $vipQuote);
        if ($idCustomer){
            $this->_customer->load($idCustomer)->setData('is_vip','1');
            $this->_customer->save();
        }
    }
}