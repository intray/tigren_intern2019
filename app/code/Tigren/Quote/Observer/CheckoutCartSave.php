<?php
namespace Tigren\Quote\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product;

/**
 * Class TigrenCartSave
 * @package Tigren\Quote\Observer
 */
class CheckoutCartSave implements ObserverInterface
{
    /**
     * @var Product
     */
    protected $_product;

    /**
     * ChangedQuote constructor.
     * @param Product $product
     */
    public function __construct(
        Product $product
    )
    {
        $this->_product = $product;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $isVip = 0;
        $cart = $observer->getData('cart');
        $cart->getQuote()->setData('is_vip',$isVip);
        $items = $cart->getQuote()->getAllVisibleItems();
        foreach ($items as $item) {
            $id_product = $item->getProductId();
            $vipProduct = $this->_product->load($id_product)->getData('is_vip');
            if ($vipProduct) {
                $isVip = 1;
                break;
            }
        }
        $cart->getQuote()->setData('is_vip',$isVip);
    }
}