<?php
namespace Tigren\FAQ\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ChangeDisplayText implements ObserverInterface{

    public function execute(Observer $observer)
    {
        $text = $observer->getData('mp_text');
        echo $text->getText() . " - Event </br>";
        return $text;
    }
}