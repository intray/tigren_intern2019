<?php

namespace Tigren\FAQ\Block;

use Magento\Framework\View\Element\Template;
use Tigren\FAQ\Helper\Data;

class Link extends \Magento\Framework\View\Element\Html\Link
{
    protected  $_helperData;

    public function __construct(
        Data $helperData,
        Template\Context $context,
        array $data = []
    )
    {
        $this->_helperData = $helperData;
        parent::__construct($context, $data);
    }

    /**
     * Render block HTML.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->_helperData->getConfigQuestion()) {
            if (false != $this->getTemplate()) {
                return parent::_toHtml();
            }
            return '<li><a ' . $this->getLinkAttributes() . ' >' . $this->escapeHtml($this->getLabel()) . '</a></li>';
        }
    }
}