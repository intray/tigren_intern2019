<?php

namespace Tigren\FAQ\Block\Faq;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Tigren\FAQ\Model\Category;
use Tigren\FAQ\Model\Faq;
use Magento\Framework\App\Request\Http;

/**
 * Class ViewAll
 * @package Tigren\FAQ\Block\Faq
 */
class ViewAll extends Template
{
    /**
     * @var
     */
    protected $_faq;

    /**
     * @var
     */
    protected $_category;

    /**
     * @var
     */
    protected $_resoure;

    /**
     * @var Http
     */
    protected $_request;

    /**
     * ViewAll constructor.
     * @param Context $context
     * @param Category $category
     * @param Faq $faq
     * @param Http $request
     * @param ResourceConnection $resource
     * @param array $data
     */

    public function __construct(
        Context $context,
        Category $category,
        Faq $faq,
        Http $request,
        ResourceConnection $resource,
        array $data = []
    )
    {
        $this->_faq = $faq;
        $this->_category = $category;
        $this->_resoure = $resource;
        $this->_request = $request;
        parent::__construct(
            $context,
            $data
        );

    }

    /**
     * @return mixed
     */
    public function _getIddata()
    {
        return $this->_request->getParam('id');
    }

    /**
     * @return \Tigren\FAQ\Model\ResourceModel\Faq\Collection|null
     */
    private function _getFaqCollection()
    {
        if ($idCategory = $this->_getIddata()) {
            return $this->_faq->getCollection()->getFaqByCategory($idCategory);
        }
        return $this->_faq->getCollection()->getFaqAll();
    }


    /**
     * @return mixed
     */
    protected function _getCategoryCollection()
    {
        return $this->_category->getCollection()->getCategoryAll();
    }

    /**
     * @return \Tigren\FAQ\Model\ResourceModel\Faq\Collection|null
     */
    public function getLoadFaqCollection()
    {
        return $this->_getFaqCollection();
    }

    /**
     * @return Collection|null
     */
    public function getLoadCategoryCollection()
    {
        return $this->_getCategoryCollection();
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('faqs/faq/');
    }

    /**
     * @param $category
     * @return string
     */
    public function getCategoryUrl($category)
    {
        if (!$category->getId()) {
            return '#';
        }
        return $this->getUrl('faqs/faq/viewall', ['id' => $category->getId()]);
    }

}