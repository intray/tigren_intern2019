<?php

namespace Tigren\FAQ\Block\Faq;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Element\Template\Context;
use Tigren\FAQ\Model\Faq;

/**
 * Class ListFaq
 * @package Tigren\FAQ\Block\Faq
 */
class ListFaq extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Faq
     */
    protected $_faq;

    /**
     * @var ResourceConnection
     */
    protected $_resource;


    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_request;

    /**
     * ListFaq constructor.
     * @param Context $context
     * @param Faq $faq
     * @param \Magento\Framework\App\Request\Http $request
     * @param ResourceConnection $resource
     * @param array $data
     */
    public function __construct(
        Context $context,
        Faq $faq,
        \Magento\Framework\App\Request\Http $request,
        ResourceConnection $resource,
        array $data = []
    )
    {
        $this->_request = $request;
        $this->_faq = $faq;
        $this->_resource = $resource;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        // You can put these informations editable on BO
        $title = __('Help Center');
        $description = __('Support everybody');
        $keywords = __('faqs,help');

        $this->getLayout()->createBlock('Magento\Catalog\Block\Breadcrumbs');

        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb(
                'faqs',
                [
                    'label' => $title,
                    'title' => $title,
                    'link' => false // No link for the last element
                ]
            );
        }
        $this->pageConfig->getTitle()->set($title);
        $this->pageConfig->setDescription($description);
        $this->pageConfig->setKeywords($keywords);


        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($title);
        }

        return $this;
    }
    /**
     * @return |null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _getKeydata()
    {

        $this->_request->getParams(); // all params
        return $this->_request->getParam('keyword');
    }
    /**
     * @return \Tigren\FAQ\Model\ResourceModel\Faq\Collection|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getFaqCollection()
    {
        if ($keyword = $this->_getKeydata()) {
            return $this->_faq->getCollection()->getFaqSearch($keyword);
        }
        return $this->_faq->getCollection()->getJoinData();
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return $this->getUrl('faqs/faq/viewall');
    }
    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('faqs/faq/index');
    }
}