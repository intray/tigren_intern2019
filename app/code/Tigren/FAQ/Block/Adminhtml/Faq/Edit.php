<?php
namespace Tigren\FAQ\Block\Adminhtml\Faq;

use Magento\Backend\Block\Widget\Form\Container;

/**
 * Class Edit
 * @package Tigren\FAQ\Block\Adminhtml\Faq
 */
class Edit extends Container
{
    /**
     * @var \Magento\Framework\Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * Edit constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'Tigren_FAQ';
        $this->_controller = 'adminhtml_faq';

        parent::_construct();

        if ($this->_isAllowedAction('Tigren_FAQ::faq_save')) {
            $this->buttonList->update('save', 'label', __('Save Question'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }
    }

    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('faqs_faq')->getId()) {
            return __("Edit Question '%1'", $this->escapeHtml($this->_coreRegistry->registry('faqs_faq')->getQuestion()));
        } else {
            return __('New Question');
        }
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('faqs/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}