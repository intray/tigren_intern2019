<?php

namespace Tigren\FAQ\Block\Adminhtml\Faq\Edit;

use Magento\Backend\Block\Template\Context;
use \Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Tigren\FAQ\Model\ResourceModel\Category\Collection;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Product;

/**
 * Class Form
 * @package Tigren\FAQ\Block\Adminhtml\Faq\Edit
 */
class Form extends Generic
{
    /**
     * @var Store
     */
    protected $_systemStore;
    /**
     * @var
     */
    protected $_store;
    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Store $systemStore
     * @param Collection $category
     * @param array $data
     */
    protected $_category;

    protected $_product;

    /**
     * Form constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Store $systemStore
     * @param FormFactory $formFactory
     * @param Collection $category
     * @param StoreManagerInterface $store
     * @param Product $product
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Store $systemStore,
        FormFactory $formFactory,
        Collection $category,
        StoreManagerInterface $store,
        Product   $product,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        $this->_store = $store;
        $this->_category = $category;
        $this->_product = $product;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('faq_form');
        $this->setTitle(__('Faq Information'));
    }

    public function selectProduct()
    {
        $options[] = ['label' => '', 'value' => ''];
        foreach ($this->_product->getCollection() as $key) {
            $options[] = [
                'value' => $key->getId(),
                'label' => $key->getSku(),
            ];
        }
        return $options;
    }
    /**
     * Prepare form
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        /** @var \Tigren\FAQ\Model\Faq $model */
        $model = $this->_coreRegistry->registry('faqs_faq');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setHtmlIdPrefix('faq_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );
        if ($model->getId()) {
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        }
        $fieldset->addField(
            'question',
            'textarea',
            ['name' => 'question', 'label' => __('Faq Content'), 'title' => __('Faq Content'), 'required' => true]
        );
        $selectProduct = $this->selectProduct();
        $fieldset->addField(
            'product_id',
            'select',
            ['name' => 'product_id', 'label' => _('Product Sku'), 'title' => _('Product'), 'required' => true, 'values' => $selectProduct, 'onclick' => "", 'onchange' => "", 'value' => '0', 'disabled' => false, 'readonly' => false,]
        );
        $fieldset->addField(
            'sender_email',
            'text',
            ['name' => 'sender_email', 'label' => __('Sender Email'), 'title' => __('Sender Email'), 'required' => true]
        );
        $fieldset->addField(
            'store_id',
            'multiselect',
            [
                'name' => 'store_id[]',
                'label' => __('Store Views'),
                'title' => __('Store Views'),
                'required' => true,
                'values' => $this->_systemStore->getStoreValuesForForm(false, true),
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            ['name' => 'status', 'label' => _('Status'), 'title' => _('Status'), 'required' => false, 'values' => array('0' => 'Pending', '1' => 'Answered'), 'onclick' => "", 'onchange' => "", 'value' => '0', 'disabled' => false, 'readonly' => false,]
        );
        $selectCategory = $this->_category->selectCategory();
        $fieldset->addField(
            'category_id',
            'select',
            ['name' => 'category_id', 'label' => _('Category'), 'title' => _('Category ID'), 'required' => false, 'values' => $selectCategory, 'onclick' => "", 'onchange' => "", 'value' => '0', 'disabled' => false, 'readonly' => false,]
        );

        $form->setValues($model->getData());
//        $form->setValues(['values' => $this->_store->getStore()->getId()]);
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}