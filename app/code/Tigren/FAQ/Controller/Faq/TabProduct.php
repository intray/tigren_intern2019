<?php

namespace Tigren\FAQ\Controller\Faq;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Tigren\FAQ\Model\Faq;
use Tigren\FAQ\Model\FaqFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class TabProduct
 * @package Tigren\FAQ\Controller\Faq
 */
class TabProduct extends Action
{
    /**
     * @var FaqFactory
     */
    protected $_faq;

    /**
     * @var ResultFactory
     */
    protected $_resultRedirect;

    protected $_storeManager;
    /**
     * Post constructor.
     * @param Context $context
     * @param Faq $faq
     * @param ResultFactory $result
     */
    public function __construct(
        Context $context,
        Faq $faq,
        StoreManagerInterface $storeManager,
        ResultFactory $result
    )
    {
        parent::__construct($context);
        $this->_faq = $faq;
        $this->_storeManager = $storeManager;
        $this->_resultRedirect = $result;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        if (!$this->getRequest()->getPostValue()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        $data = $this->getRequest()->getPostValue();
        $id_store = $this->_storeManager->getStore()->getId();
        /** @var \Tigren\FAQ\Model\Faq $faq */
        $model = $this->_faq;
        $model->setData([
            'question' => $data['question'],
            'sender_email' => $data['sender_email'],
            'product_id' => $data['product_id'],
            'store_id' => $id_store,
        ]);
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $model->save();
            $this->messageManager->addSuccess(__('Question Sucessful.'));
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
