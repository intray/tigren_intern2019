<?php

namespace Tigren\FAQ\Controller\Adminhtml\FAQ;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Tigren_FAQ::faq';

    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Tigren_FAQ::faq');
        $resultPage->addBreadcrumb(__('faqs'), __('question'));
        $resultPage->getConfig()->getTitle()->prepend(__('INTRAY RIGHT HAND'));

        return $resultPage;
    }
}