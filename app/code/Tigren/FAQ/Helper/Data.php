<?php

namespace Tigren\FAQ\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Tigren\FAQ\Model\ResourceModel\Faq\Collection;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Data
 * @package Tigren\FAQ\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var Collection
     */
    protected $_faqCollection;
    /**
     * @var Session
     */
    protected $_customer;
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     *
     */
    const QUESTION_ENABLED = 'faqs/faq/enable_module';

    /**
     * Data constructor.
     * @param Collection $faqCollection
     * @param Session $customer
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Collection $faqCollection,
        Session $customer,
        Context $context,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_customer = $customer;
        $this->_scopeConfig = $scopeConfig;
        $this->_faqCollection = $faqCollection;
        parent::__construct($context);
    }

    /**
     * @return Collection
     * @throws NoSuchEntityException
     */
    public function getFaqStore()
    {
        return $this->_faqCollection->getJoinData();
    }

    /**
     * @param $productId
     * @return Collection
     * @throws NoSuchEntityException
     */
    public function getFaqProduct($productId)
    {
        return $this->getFaqStore()->addFieldToFilter('product_id', array('eq' => $productId));
    }

    /**
     * @return bool
     */
    public function checkCustomer()
    {
        return $this->_customer->isLoggedIn();
    }

    /**
     * @return mixed
     */
    public function getConfigQuestion()
    {
        return $this->_scopeConfig->getValue(self::QUESTION_ENABLED);
    }
}