<?php
namespace Tigren\FAQ\Model;

use \Magento\Framework\Model\AbstractModel;

class Faq extends AbstractModel
{
    const FAQ_ID = 'entity_id'; // We define the id fieldname

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'faqs'; // parent value is 'core_abstract'

    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'faq'; // parent value is 'object'

    /**
     * Name of object id field
     *
     * @var string
     protected $_idFieldName = self::FAQ_ID; // parent value is 'id'

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\FAQ\Model\ResourceModel\Faq');
    }

    public function getEnableStatus() {
        return 1;
    }

    public function getDisableStatus() {
        return 0;
    }
}