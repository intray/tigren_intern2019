<?php

namespace Tigren\FAQ\Model\ResourceModel\Faq;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Tigren\FAQ\Model\Faq;

/**
 * Class Collection
 * @package Tigren\FAQ\Model\ResourceModel\Faq
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = Faq::FAQ_ID;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Define resource
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param AdapterInterface|null $connection
     * @param AbstractDb|null $resource
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        ManagerInterface $eventManager,
        FetchStrategyInterface $fetchStrategy,
        LoggerInterface $logger,
        StoreManagerInterface $storeManager,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct(
            $entityFactory, $logger,
            $fetchStrategy, $eventManager,
            $connection, $resource
        );
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStore()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Tigren\FAQ\Model\Faq', 'Tigren\FAQ\Model\ResourceModel\Faq');
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getJoinData()
    {
        $id_store = $this->getStore();
        $this->getSelect()->join(
            'tigren_storeview', 'main_table.entity_id = tigren_storeview.faq_id'
        )->where("store_id = '" . $id_store . "'");
        return $this;
    }

    public function getJoinById(){

        $this->getSelect()->join(
            'tigren_storeview', 'main_table.entity_id = tigren_storeview.faq_id'
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function getFaqAll()
    {
        return $this;
    }

    /**
     * @param $key
     * @return Collection
     */
    public function getFaqSearch($key)
    {
        return $this->addFieldToFilter('question', array('like' => '%' . $key . '%'));
    }

    /**
     * @param $key
     * @return Collection
     */
    public function getFaqByCategory($key)
    {
        return $this->addFieldToFilter('category_id', $key);
    }
}