<?php

namespace Tigren\FAQ\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use \Magento\Framework\Model\AbstractModel;

/**
 * Faq post mysql resource
 */
class Faq extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        // Table Name and Primary Key column
        $this->_init('tigren_faq', 'entity_id');
    }

    /**
     * @param AbstractModel $object
     * @return void
     */
    protected function saveStore($object)
    {
        foreach ((array)$object->getData('store_id') as $store) {
            $storeArray = [
                'faq_id' => $object->getId(),
                'store_id' => $store,
            ];
            $this->getConnection()->insert(
                $this->getTable('tigren_storeview'),
                $storeArray
            );
        }
    }

    /**
     * @param AbstractModel $object
     * @return AbstractDb
     */
    protected function _afterSave(AbstractModel $object)
    {
        $this->saveStore($object);
        return parent::_afterSave($object);
    }

    public function loadStore(\Magento\Framework\Model\AbstractModel $object)
    {
        $select = $this->getConnection()->select()
            ->from($this->getTable('tigren_storeview'))
            ->where('faq_id = ?', $object->getId());

        if ($data = $this->getConnection()->fetchAll($select)) {
            $array = [];
            foreach ($data as $row) {
                $array[] = $row['store_id'];
            }
            $object->setData('store_id', $array);
        }

        return $object;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->loadStore($object);
        return parent::_afterLoad($object);
    }
}