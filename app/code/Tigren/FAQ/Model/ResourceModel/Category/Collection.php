<?php

namespace Tigren\FAQ\Model\ResourceModel\Category;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Tigren\FAQ\Model\ResourceModel\Category
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = \Tigren\FAQ\Model\Faq::FAQ_ID;

    /**
     * Define resource
     * @return voidmodel
     *
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    )
    {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Tigren\FAQ\Model\Category', 'Tigren\FAQ\Model\ResourceModel\Category');
    }

    /**
     * @return array
     */
    public function selectCategory()
    {
        $options[] = ['label' => '', 'value' => ''];
        foreach ($this as $key) {
            $options[] = [
                'value' => $key->getId(),
                'label' => $key->getTitle(),
            ];
        }
        return $options;
    }

    /**
     * @return $this
     */
    public function getCategoryAll()
    {
        return $this;
    }
}