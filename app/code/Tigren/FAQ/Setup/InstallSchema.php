<?php namespace Tigren\FAQ\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'tigren_faq'
         */

        $tableName = $installer->getTable('tigren_faq');
        $tableComment = 'Question management for faq module';
        $columns = array(
            'entity_id' => array(
                'type' => Table::TYPE_INTEGER,
                'size' => null,
                'options' => array('identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true),
                'comment' => 'Question Id',
            ),
            'question' => array(
                'type' => Table::TYPE_TEXT,
                'size' => 255,
                'options' => array('nullable' => false, 'default' => ''),
                'comment' => 'Content question',
            ),
            'sender_email' => array(
                'type' => Table::TYPE_TEXT,
                'size' => 200,
                'options' => array('nullable' => false, 'default' => ''),
                'comment' => 'sender email',
            ),
            'status' => array(
                'type' => Table::TYPE_BOOLEAN,
                'size' => 20,
                'options' => array('nullable' => false, 'default' => '0'),
                'comment' => 'quesion status',
            ),
            'category_id' => array(
                'type' => Table::TYPE_INTEGER,
                'size' => null,
                'options' => array('unsigned' => true, 'nullable' => false),
                'comment' => 'category ID',
            ),
            'product_id' => array(
                'type' => Table::TYPE_INTEGER,
                'size' => null,
                'options' => array('unsigned' => true, 'nullable' => false),
                'comment' => 'product ID',
            ),
        );

        $indexes =  array(
        );

        $foreignKeys = array(
        );

        /**
         *  We can use the parameters above to create our table
         */

        // Table creation
        $table = $installer->getConnection()->newTable($tableName);

        // Columns creation
        foreach($columns AS $name => $values){
            $table->addColumn(
                $name,
                $values['type'],
                $values['size'],
                $values['options'],
                $values['comment']
            );
        }

        // Indexes creation
        foreach($indexes AS $index){
            $table->addIndex(
                $installer->getIdxName($tableName, array($index)),
                array($index)
            );
        }

        // Foreign keys creation
        foreach($foreignKeys AS $column => $foreignKey){
            $table->addForeignKey(
                $installer->getFkName($tableName, $column, $foreignKey['ref_table'], $foreignKey['ref_column']),
                $column,
                $foreignKey['ref_table'],
                $foreignKey['ref_column'],
                $foreignKey['on_delete']
            );
        }

        // Table comment
        $table->setComment($tableComment);

        // Execute SQL to create the table
        $installer->getConnection()->createTable($table);

        // End Setup
        $installer->endSetup();
    }

}