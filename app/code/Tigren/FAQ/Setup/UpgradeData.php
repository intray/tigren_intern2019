<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\FAQ\Setup;

use Tigren\FAQ\Model\Faq;
use Tigren\FAQ\Model\Category;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{

    protected $_faq;

    public function __construct(Faq $faq, Category $category){
        $this->_faq = $faq;
        $this->_category = $category;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        // Action to do if module version is less than 1.0.0.0
        if (version_compare($context->getVersion(), '1.0.0.1') < 0) {
            $faqs = [
                [
                    'question' => 'Do you www',
                    'sender_email' => 'admin1@gamail.com',
                    'store_view' => '1',
                    'status' => '1',
                    'category_id' => '1'
                ],
                [
                    'question' => 'Do you aaa',
                    'sender_email' => 'admin2@gamail.com',
                    'store_view' => '1',
                    'status' => '0',
                    'category_id' => '2'
                ],
                [
                    'question' => 'Do you bbb',
                    'sender_email' => 'admin3@gamail.com',
                    'store_view' => '2',
                    'status' => '1',
                    'category_id' => '3'
                ],
                [
                    'question' => 'Do you ccc',
                    'sender_email' => 'admin4@gamail.com',
                    'store_view' => '2',
                    'status' => '0',
                    'category_id' => '2'
                ],
            ];

            /**
             * Insert faq
             */

            foreach ($faqs as $data) {
                $this->_faq->setData($data)->save();
            }


            $categorys = [
                [
                    'title' => 'Date $ Sale',
                    'url_key' => 'date_and_sale',
                    'status' => '1',
                    'position' => '1'
                ],
                [
                    'title' => 'Color and Sizes',
                    'url_key' => 'color_and_size',
                    'status' => '0',
                    'position' => '3'
                ],
                [
                    'title' => 'Delivery Question',
                    'url_key' => 'delivery_question',
                    'status' => '1',
                    'position' => '5'
                ],
            ];

            /**
             * Insert categorys
             */

            foreach ($categorys as $data) {
                $this->_category->setData($data)->save();
            }
        }

        $installer->endSetup();
    }
}