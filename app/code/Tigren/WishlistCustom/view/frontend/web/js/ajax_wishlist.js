define([
    "jquery",
    "mage/url",
    "mage/template",
    "jquery/ui",
    "Magento_Ui/js/modal/modal",
], function($) {
    "use strict";

    //creating jquery widget
    $.widget('mage.ajaxWishlist', {
        options: {
            modalForm: '#popup-modal'
        },
        _create: function() {
            var seft = this;
            this.options.modalOption = this._getModalOptions();
            var modalOption = this.options.modalOption;
            var modalForm = this.options.modalForm;
            var element = this.element;
            element.on('click', function(e){
                // var span = document.querySelector('.towishlist');
                // console.log(span);
                var productData = JSON.parse(element.attr('data-post'));
                var productId = productData.data.product;
                e.stopPropagation();
                $('body').trigger('processStart');

                $.ajax({
                    url: seft.options.url,
                    type: "POST",
                    data: {
                        productid: productId
                    },
                    dataType: "json"
                }).done(function (data) {
                    $('body').trigger('processStop');
                    $('#popup-modal .contentTg').html(data.html);
                    $('#product-wraper-options').trigger('contentUpdated');
                    // Initialize modal
                    $(modalForm).modal(modalOption);
                    $(".modal-footer").hide();
                    //open modal
                    $(modalForm).trigger('openModal');
                });
            });
        },
        _getModalOptions: function() {
            /**
             * Modal options
             */
            var options;
            options = {
                type: 'popup',
                responsive: true
            };
            return options;
        },
    });
    return $.mage.ajaxWishlist;
});
