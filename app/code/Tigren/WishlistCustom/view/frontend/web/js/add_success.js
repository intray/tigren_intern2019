define([
    "jquery",
    "jquery/ui"
], function($) {
    "use strict";

    //creating jquery widget
    $.widget('mage.addSuccess', {
        _create: function() {
            this.element.on('click', function(e){
                console.log('Click ME!')
            });
        }
    });
    return $.mage.addSuccess;
});