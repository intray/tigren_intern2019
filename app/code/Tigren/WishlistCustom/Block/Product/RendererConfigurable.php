<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Tigren\WishlistCustom\Block\Product;
class RendererConfigurable extends \Magento\Swatches\Block\Product\Renderer\Configurable
{
  
    const SWATCH_RENDERER_TEMPLATE_CUSTOM= 'Tigren_WishlistCustom::renderer.phtml';

  protected function getRendererTemplate()
    {
        return $this->isProductHasSwatchAttribute() ?
            self::SWATCH_RENDERER_TEMPLATE_CUSTOM : self::CONFIGURABLE_RENDERER_TEMPLATE;
    }
}
