<?php
namespace Tigren\WishlistCustom\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Model\Product;
use Magento\Wishlist\Controller\WishlistProviderInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\Registry;

/**
 * Class AddWishlist
 * @package Tigren\WishlistCustom\Controller\Index
 */
class AddWishlist extends Action
{
    /**
     * @var Session
     */
    protected $_custumer;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var Product
     */
    protected $_product;

    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var WishlistProviderInterface
     */
    protected $wishlistProvider;

    /**
     * @var Registry
     */
    protected $_registry;
    /**
     * @var Context
     */
    private $context;

    /**
     * AddWishlist constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Product $product
     * @param WishlistProviderInterface $wishlistProvider
     * @param LayoutFactory $resultLayoutFactory
     * @param Registry $registry
     * @param Session $customer
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Product $product,
        WishlistProviderInterface $wishlistProvider,
        LayoutFactory $resultLayoutFactory,
        Registry $registry,
        Session $customer
    )
    {
        parent::__construct($context);
        $this->_product = $product;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->wishlistProvider = $wishlistProvider;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->_custumer = $customer;
        $this->_registry = $registry;
        $this->context = $context;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $customerId = $this->_custumer->getCustomerId();
        if (!$customerId) {
            $layout = $this->resultLayoutFactory->create();
            $layout->getUpdate()->load(['sign_in']);
            $layout->generateXml();
            $layout->generateElements();
            $html = $layout->getOutput();
            $data['html'] = $html;
            $result->setData($data);
        }
       else {
           $productId = $this->getRequest()->getParam('productid');
           $product = $this->_product->load($productId);
           $this->_registry->register('product', $product);
           $this->_registry->register('current_category', $product);
           $layout = $this->resultLayoutFactory->create();
           $layout->getUpdate()->load(['add_product']);
           $layout->generateXml();
           $layout->generateElements();
           $html = $layout->getOutput();
           $data['html'] = $html;
           $result->setData($data);
        }
        return $result;
    }
}