<?php

namespace Tigren\WishlistCustom\Controller\Index;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException as NoSuchEntityExceptionAlias;
use Magento\Wishlist\Controller\WishlistProviderInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Registry;

/**
 * Class AddWishlistSuccess
 * @package Tigren\WishlistCustom\Controller\Index
 */
class AddWishlistSuccess extends Action
{
    /**
     * @var WishlistProviderInterface
     */
    protected $wishlistProvider;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * AddWishlistSuccess constructor.
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param WishlistProviderInterface $wishlistProvider
     * @param LayoutFactory $resultLayoutFactory
     * @param Registry $registry
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        WishlistProviderInterface $wishlistProvider,
        LayoutFactory $resultLayoutFactory,
        Registry $registry,
        JsonFactory $resultJsonFactory
    )
    {
        parent::__construct($context);
        $this->wishlistProvider = $wishlistProvider;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_registry = $registry;
        $this->productRepository = $productRepository;
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws LocalizedException
     * @throws NoSuchEntityExceptionAlias
     * @throws Exception
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $requestParams = $this->getRequest()->getParams();
        if (isset($requestParams['super_attribute'])) {
            $productOption = $requestParams['super_attribute'];
            $productId = $requestParams['product'];
            $product = $this->productRepository->getById($productId);
            $optionSelect = '';
            foreach ($productOption as $key => $value) {
                $attribute = $product->getResource()->getAttribute($key);
                $nameAttribute = $attribute->getFrontend()->getLabel($product);
                $valueAttribute = $attribute->getSource()->getOptionText($value);
                if ($valueAttribute) {
                    $optionSelect .= "<p>" . $nameAttribute . ":" . $valueAttribute . "</p>";
                }
            }
        }else{
            $productId = $this->getRequest()->getParam('product');
            $product = $this->productRepository->getById($productId);
            $optionSelect = '';
        }

        $this->_registry->register('product', $product);
        $this->_registry->register('current_product', $product);
        $wishlist = $this->wishlistProvider->getWishlist();
        $buyRequest = new DataObject($requestParams);
        $wishlist->addNewItem($product, $buyRequest);
        $wishlist->save();

        $layout = $this->resultLayoutFactory->create();
        $layout->getUpdate()->load(['popup_success']);
        $layout->generateXml();
        $layout->generateElements();
        $html = $layout->getOutput();
        $optionSuccess = "<div class='attribute-product-tg'>" . $optionSelect . "</div>";
        $data['html'] = $html . $optionSuccess;
        $result->setData($data);
        return $result;
    }
}